ATmega32u4 example - hello world
================================

  -- Hello World example for the ATmega32u4 on an Arduino Micro

This file is part of `ATmega32u4_c-example_hellow-world`

Copyright (c) 2017 Martin Singer <martin.singer@web.de>


About
=====

### Arduino Micro

* <https://www.arduino.cc/en/Main/ArduinoBoardMicro>


#### Pin Mapping

![Image: Arduino Micro pin mapping](./img/arduino_micro_pin_mapping.png "Arduino Micro pin mapping")

* <https://www.arduino.cc/en/uploads/Main/ArduinoMicro_Pinout3.png>
* <https://www.arduino.cc/en/Hacking/PinMapping32u4>


### udev

Setup a udev rule with unique identifiers for the programmer device!
This can be useful if you have more, similar USB devices connected.

1. Double press the reset button of the Arduino Micro
2. `$ udevadm info -a -p $(udevadm info -q path -n /dev/ttyACM0)`
   *(ensure your Arduino connects on `/dev/ttyACM0`)*
3. Checkout your device attributes
4. `# echo 'KERNEL=="ttyACM[0-9]", SUBSYSTEMS=="usb", ATTRS{idVendor}=="2a03", ATTRS{idProduct}=="0037", ATTRS{bcdDevice}=="0001", SYMLINK+="micro"' >> /etc/udev/rules.d/50-arduino.rules`
5. `$ udevadm control --reload` or `$ udevadm trigger`


Requirements
------------

### General

* avr-gcc
* avrdude


Documentation
-------------

	$ doxygen doxygen.conf
	$ firefox doc/html/index.html


Usage
-----

* `make all`        Make software.
* `make clean`      Clean out built project files.
* `make program`    Download the hex file to the device, using avrdude.


### TEST functions

1. Blinking orange onboard RX LED (no external circuit required)
2. External LED blinks when external KEY is pressed


	$ make clean && make TEST=1
	$ make program


Before programming the Arduino Micro double press the reset button!


Circuits
--------

### TEST 1: Blinking orange onboard RX LED (no external circuit required)

*no external circuit required*


### TEST 2: External LED blinks when external KEY is pressed


![Image: Circuit TEST 2](./img/circuit.jpg "Circuit TEST 2")


* R1: Resistor 1 - ??? ohm
* R2: Resistor 2 - ??? ohm
* K1: Key 1      - Type ???
* D1: LED 1      - Type ???


* The key is connected on Arduino Digital Pin 4
* The LED is connected on Arduino Digital Pin 6

